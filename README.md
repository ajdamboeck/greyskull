# Greyskull 0.6.1

Sveltekit Frontend for
[`ZOAR`](https://gitlab.com/ajdamboeck/zoar)
via Houdini/GraphQL.

## Setup

```bash
# clone the project
git clone git@gitlab.com:ajdamboeck/grayskull.git && cd greyskull && npm i

# generate houdini runtime
 npx houdini generate -h Authorization="Bearer SECRETTOKEN"
```

## Docs

- [`Sveltekit Docs`](https://kit.svelte.dev/docs/introduction)
- [`Houdini Docs`](https://www.houdinigraphql.com/guides/setting-up-your-project)
- [`Directus Docs`](https://docs.directus.io/)

## Developing

Once you've created a project and installed dependencies with `npm install` (or `pnpm install` or `yarn`), start a development server:

```bash
npm run dev
```

## Building

To create a production version of your app:

```bash
npm run build
```

You can preview the production build with `npm run preview`.

> To deploy your app, you may need to install an [adapter](https://kit.svelte.dev/docs/adapters) for your target environment.
