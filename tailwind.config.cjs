import typography from '@tailwindcss/typography'
import daisyui from 'daisyui'

const config = {
	content: ['./src/**/*.{html,js,svelte,ts}'],

	theme: {
		extend: {}
	},
	typography: {
		default: {
			css: {
				maxWidth: '85ch'
			}
		}
	},
	daisyui: {
		themes: [
			{
				light: {
					primary: '#9F2D14',
					'primary-content': '#FAF2EB',
					secondary: '#E28736',
					'secondary-content': '#221911',
					accent: '#3D405C',
					'accent-content': '#ACA7BE',
					neutral: '#FAF2EB',
					'neutral-content': '#221911',
					'base-100': '#FAF2EB',
					'base-content': '#221911',
					'.logo svg :where(#path685)': {
						'@apply fill-accent': ''
					},
					'.logo svg :where(#path833)': {
						'@apply fill-accent-content': ''
					},
					info: '#3ABFF8',
					success: '#36D399',
					warning: '#FBBD23',
					error: '#F87272',
					'--rounded-box': '0',
					'--rounded-btn': '0',
					'--rounded-badge': '0',
					'--tab-radius': '0'
				},
				dark: {
					primary: '#8CA6D9',
					'primary-content': '#181221',
					secondary: '#393267',
					'secondary-content': '#8CA6D9',
					accent: '#E4D481',
					'accent-content': '#393267',
					neutral: '#181221',
					'neutral-content': '#FAF2EB',
					'base-100': '#181221',
					'base-content': '#FAF2EB',
					info: '#8CA6D9',
					success: '#36D399',
					warning: '#FBBD23',
					error: '#F87272',
					'--rounded-box': '0',
					'--rounded-btn': '0',
					'--rounded-badge': '0',
					'--tab-radius': '0'
				}
			}
		]
	},
	plugins: [typography, daisyui]
}

module.exports = config
