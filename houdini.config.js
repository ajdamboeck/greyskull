/** @type {import('houdini').ConfigFile} */
const config = {
	defaultFragmentMasking: 'disable',
	defaultCachePolicy: 'CacheOrNetwork',
	logLevel: 'full',
	plugins: {
		'houdini-svelte': {
			client: './src/client'
		}
	},
	watchSchema: {
		url: 'env:API_URL',
		interval: 6000,
		headers: {
			Authentication(env) {
				return `Bearer ${env.API_KEY}`
			}
		}
	},
	scalars: {
		Date: {
			type: 'Date',
			marshal(val) {
				return val.getTime()
			},
			unmarshal(val) {
				return new Date(val)
			}
		},
		JSON: {
			// the corresponding typescript type
			type: 'unknown',
			// turn the api's response into that type
			unmarshal(val) {
				return val
			},
			// turn the value into something the API can use
			marshal(json) {
				return json.toString()
			}
		}
	}
}

export default config
