import preprocess from 'svelte-preprocess'
import adapter from '@sveltejs/adapter-auto'
import path from 'path'
import { vitePreprocess } from '@sveltejs/kit/vite'

/** @type {import('@sveltejs/kit').Config} */
const config = {
	// Consult https://github.com/sveltejs/svelte-preprocess
	// for more information about preprocessors
	preprocess: [
		vitePreprocess(),
		preprocess({
			postcss: true
		})
	],
	kit: {
		prerender: {
			handleMissingId: 'ignore'
		},
		adapter: adapter(),
		alias: {
			$houdini: path.resolve('.', '$houdini'),
			$lib: path.resolve('./src/lib'),
			$components: path.resolve('./src/components'),
			$generics: path.resolve('./src/components/generics')
		}
	},
	vitePlugin: {
		inspector: true
	}
}

export default config
