import { graphql, type ConventionBySlug$input } from '$houdini'
import type { LoadEvent } from '@sveltejs/kit'

export const _houdini_load = graphql`
	query ConventionBySlug($slug: String!) {
		conventions(filter: { slug: { _eq: $slug } }, limit: 1) {
			id
			name
			untertitel
			intro
			text
			start
			ende
		}
	}
`
export function _ConventionBySlugVariables(event: LoadEvent): ConventionBySlug$input {
	const { slug } = event.params
	return {
		slug: slug ? slug : ''
	}
}
