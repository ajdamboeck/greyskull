import { HoudiniClient } from '$houdini'
import { env } from '$env/dynamic/public'

const auth = env.PUBLIC_API_KEY
const apiUrl = env.PUBLIC_API_URL

export default new HoudiniClient({
	url: apiUrl,
	fetchParams() {
		return {
			headers: {
				Authentication: `Bearer ${auth}`
			}
		}
	}
})
