// graphql.config.js
module.exports = {
	projects: {
		app: {
			schema: ['$houdini/graphql/schema.graphql'],
			documents: ['$houdini/graphql/documents.gql']
		}
	}
}
